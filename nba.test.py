import unittest
import nba


class TestNBA(unittest.TestCase):
    def setUp(self):
        self.players = nba.get_players()

    # When the input is less than any sum of heights
    def test_no_results(self):
        for i in range(0, 139):
            self.assertEqual(nba.my_solution(i, self.players), [])

    # When the input is more than any sum of heights
    def test_no_results_2(self):
        for i in range(178, 200):
            self.assertEqual(nba.my_solution(i, self.players), [])

    # Compare my results with the naive implementation
    def test_equal_results(self):
        for i in range(139, 178):
            self.assertEqual(
                sorted(nba.my_solution(i, self.players)),
                sorted(nba.naive_solution(i, self.players)),
            )

    # Test for duplicates
    def test_unique_pairs(self):
        for i in range(139, 178):
            pairs = nba.my_solution(i, self.players)
            names = [n[0] + n[1] for n in pairs]
            self.assertEqual(len(names), len(set(names)))


if __name__ == "__main__":
    unittest.main()

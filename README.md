# Mach Eight Sample Project

Hi! 

This is my submission for the test. Thanks for your consideration!

Kind Regards,

David Naranjo  
me@davidnaranjo.co  
[Portfolio](https://davidnaranjo.co)

## The Problem

>Find all pairs of NBA players whose height (in inches) adds up to a given integer.

There are 435 players in the provided file. Possible pairs are between 1 and 6965, depending on the number given as input:

![Histogram of pairs](images/histogram.png)

## About this submission

The file that should interest you is `nba.py` on the project root. My submission is the `my_solution` function, that returns a list of pairs efficiently. 

The other functions below are:
* An implementation of a suboptimal O(n^2) algorithm, `naive_solution`, for comparison.
* A helper function, `format_str`, for printing the pairs list in the required format.
* Boilerplate code for running on the CLI, as well as fetching the JSON file with player info.

### Requirements
* A recent version of Python
* __OPTIONAL:__ [requests](https://pypi.org/project/requests/) library, only if you don't have/want the `players.json` file that is located in the `data` directory.

### Running from the command line

Just run the file `nba.py` with the desired number as argument:

```
> python nba.py 139
- Brevin Knight	Nate Robinson
- Mike Wilks	Nate Robinson

```

You can also add the argument `-h` for displaying a help message:

```
> python nba.py -h
usage: nba.py [-h] [--performance] input_height

positional arguments:
  input_height   an integer for which the pairs will be calculated

optional arguments:
  -h, --help     show this help message and exit
  --performance  run both naive implementation and efficient implementation, and print the running time for each one
```

### Running as a module

You can also import the `nba` module to use it in other python scripts, provided that `nba.py` is in the same folder. To invoke my solution, just use the `my_solution` function. 

```python
import nba

players = nba.get_players()
print(nba.format_str(nba.my_solution(139,players)))
```

### Testing

You can execute all unit tests by running the file `nba.test.py`.

```
> python nba.test.py --verbose
```

This should be your output:

```
> python nba.test.py --verbose
test_equal_results (__main__.TestNBA) ... ok
test_no_results (__main__.TestNBA) ... ok
test_no_results_2 (__main__.TestNBA) ... ok
test_unique_pairs (__main__.TestNBA) ... ok

----------------------------------------------------------------------
Ran 4 tests in 7.519s

```
### Performance

You can add another argument, `--performance`, if you want to evaluate the running time of my solution _vs_ a naive one of O(n^2) complexity:

```
> python nba.py 139 --performance
==========
Running naive O(n²) solution
==========
Elapsed time: 16.058757 ms
----------
- Brevin Knight	Nate Robinson
- Mike Wilks	Nate Robinson

==========
Running my solution...
==========
Elapsed time: 0.132982 ms
----------
- Brevin Knight	Nate Robinson
- Mike Wilks	Nate Robinson

```

This is a graph of the performance of my algorithm:

![Histogram of pairs](images/performance_mine.png)

And this is the comparison of a naive algorithm with complexity O(n^2):

![Histogram of pairs](images/performance_comparison.png)

### My solution

For your convenience, this is my algorithm (28 lines) without comments:

```python
def my_solution(input_height: int, players: list):
    pairs = []
    heights = {}

    for p in players:
        h = int(p["h_in"])
        full_name = f"{p['first_name']} {p['last_name']}"
        heights[h] = heights.get(h, []) + [full_name]

    min_h = min(heights.keys())

    for i in range(min_h, input_height - min_h + 1):
        if i in heights:
            for j in range(len(heights[i])):
                for k in range(i, input_height - min_h + 1):
                    if k in heights:
                        for l in range(len(heights[k])):
                            if i + k == input_height:
                                if j != l or heights[i][j] != heights[k][l]:
                                    pair = sorted(
                                        [
                                            f"{heights[i][j]}",
                                            f"{heights[k][l]}",
                                        ]
                                    )
                                    if pair not in pairs:
                                        pairs.append(pair)

    return pairs
```

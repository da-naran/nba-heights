def my_solution(input_height: int, players: list):
    """
    Efficient solution of NBA height problem.

    It searches for all pairs of players whose height in inchess
    adds up to a given height.

    Parameters:
    input_height (int): An integer for which the pairs will be calculated
    players (list): A list of dicts with player information

    Returns:
    list: A list of found pairs, empty if no pairs found
    """
    pairs = []
    heights = {}

    for p in players:
        h = int(p["h_in"])
        full_name = f"{p['first_name']} {p['last_name']}"
        heights[h] = heights.get(h, []) + [full_name]

    min_h = min(heights.keys())

    for i in range(min_h, input_height - min_h + 1):
        if i in heights:
            for j in range(len(heights[i])):
                for k in range(i, input_height - min_h + 1):
                    if k in heights:
                        for l in range(len(heights[k])):
                            if i + k == input_height:
                                if j != l or heights[i][j] != heights[k][l]:
                                    pair = sorted(
                                        [
                                            f"{heights[i][j]}",
                                            f"{heights[k][l]}",
                                        ]
                                    )
                                    if pair not in pairs:
                                        pairs.append(pair)

    return pairs


def naive_solution(input_height: int, players: list):
    """
    Inefficient solution of NBA height problem.

    It searches for all pairs of players whose height in inchess
    adds up to a given height.

    Parameters:
    input_height (int): An integer for which the pairs will be calculated
    players (list): A list of dicts with player information

    Returns:
    list: A list of found pairs, empty if no pairs found

    """
    pairs = []

    for i in range(len(players) - 1):
        for j in range(i + 1, len(players)):
            if int(players[i]["h_in"]) + int(players[j]["h_in"]) == input_height:
                pair = sorted(
                    [
                        f"{players[i]['first_name']} {players[i]['last_name']}",
                        f"{players[j]['first_name']} {players[j]['last_name']}",
                    ]
                )
                pairs.append(pair)

    return pairs


def format_str(pairs: list):
    NO_MATCH = "No matches found"
    message = ""
    if len(pairs) > 0:
        for p in pairs:
            p = sorted(p)
            message += f"- {p[0]}\t{p[1]}\n"
    else:
        message = NO_MATCH
    return message


# CODE FOR RUNNING ON CLI
import time
import argparse
import json
from os.path import exists
from os import mkdir


def execute(function, args):
    start = time.perf_counter_ns()
    return_val = function(*args)
    end = time.perf_counter_ns()
    print(f"Elapsed time: {(end-start)/1000000} ms")
    print("-" * 10)
    return return_val


def get_players():
    JSON_FILENAME = "data/players.json"
    API_URL = "https://mach-eight.uc.r.appspot.com"
    players = []
    if exists(JSON_FILENAME):
        with open(JSON_FILENAME) as file:
            object = json.load(file)
            players = object["values"]
    else:
        print(
            "Could not find JSON file", JSON_FILENAME, ". Fetching from", API_URL, "..."
        )
        # Use external dependency only if file does not exist
        import requests
        from requests.exceptions import HTTPError

        if not (exists("data")):
            mkdir("data")

        try:
            response = requests.get(API_URL)
            response.raise_for_status()
            object = response.json()
            players = object["values"]
            with open(JSON_FILENAME, "w") as f:
                json.dump(object, f)
        except HTTPError as http_err:
            print(f"HTTP error: {http_err}")
        except Exception as err:
            print(f"General error: {err}")
    return players


def main():
    ap = argparse.ArgumentParser()
    ap.add_argument(
        "input_height",
        help="an integer for which the pairs will be calculated",
        type=int,
    )
    ap.add_argument(
        "--performance",
        action="store_true",
        help="run both naive implementation and efficient implementation, and print the running time for each one",
    )
    args = vars(ap.parse_args())

    players = get_players()

    if args["performance"]:
        print("=" * 10)
        print("Running naive O(n²) solution")
        print("=" * 10)
        pairs_n = execute(naive_solution, [args["input_height"], players])
        print(format_str(pairs_n))

        print("=" * 10)
        print("Running my solution...")
        print("=" * 10)
        pairs_m = execute(my_solution, [args["input_height"], players])
        print(format_str(pairs_m))
    else:
        pairs = my_solution(args["input_height"], players)
        print(format_str(pairs))


if __name__ == "__main__":
    main()
